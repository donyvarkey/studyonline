var express = require('express');
var router = express.Router();
var Course = require('../models/course');
var UserCourse = require('../models/appliedCourses');

router.get('/courseList', function(req, res){
	res.render('courseList');
});
router.get('/userCourses', function(req, res){
	res.render('userCourses');
});



router.post("/addCourse",function(req, res){
	var courseName = req.body.courseName;
	//console.log(courseName);
	var courseDuration = req.body.courseDuration;
	var instructor = req.body.instructor;


	var newCourse = new Course({
		course_name: courseName,
		course_dur: courseDuration,
		instructor: instructor,
	});
	Course.insertOne(newCourse , function(err , course){
		if(err) throw err;
		console.log('Course Added.');
	});
	res.render('admin_dash');
	req.flash('success_msg','Course Added.');
});


router.post("/courseList" , function(req , res){
	Course.find({}, function(err, courses) {
	  if (err) throw err;
	  // object of all the users
	  //console.log(courses);
	  res.render('courseList',{data:courses});
	});
});

router.post("/userCourses" , function(req, res){
	UserCourse.find({}, function(err, courses){
		if(err) throw err;
		res.render('userCourses', {data:courses});
	});
});

module.exports = router;