var express = require('express');
var router = express.Router();


//Get homepage
router.get('/', ensureAuthenticated, function(req, res){
	if(req.user.user_type == 30){
		res.render('admin_dash');
	}
	else{
		res.render('index');
	}
});

function ensureAuthenticated(req ,res, next){
	if(req.isAuthenticated()){
		return next();
	}
	else{
		res.redirect('/users/login');
	}
}

module.exports = router;		