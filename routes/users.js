var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy; 

var User = require('../models/user');
var Course = require('../models/course');
var UserCourse = require('../models/appliedCourses');



//Register page
router.get('/register', function(req, res){
	res.render('register');
});

//User Login page
router.get('/login', function(req, res){
	res.render('login');
});

//Apply Course page
router.get('/apply', function(req, res){
	res.render('apply');
});

//Student List page
router.get('/studentList', function(req, res){
	res.render('studentlist');
});


//Register User
router.post('/register', function(req, res){
	var name = req.body.name;					//Pass form value to the server
	var email = req.body.email;
	var username = req.body.username;
	var password = req.body.password;
	var password2 = req.body.password2;

	//console.log(name);
	//Validation 
	req.checkBody('name' , 'Name is required!').notEmpty();
	req.checkBody('username' , 'Username cannot be empty').notEmpty();
	req.checkBody('email' , 'Email cannot empty!').notEmpty();
	req.checkBody('email' , 'Invalid Email').isEmail();
	req.checkBody('password' , 'Password cannot be empty').notEmpty();
	req.checkBody('password2' , 'Password do not match').equals(req.body.password);

	var errors = req.validationErrors();
	if(errors) {
		res.render('register' , {
			errors:errors
		});
	}
	else{
		var newUser = new User({
			name: name,
			username: username,
			email: email,
			password: password,
		});

		User.createUser(newUser, function(err , user){
			if(err) throw err;
		});
		req.flash('success_msg' , 'You can now login.');
		res.redirect('/users/login');

		// var adminUser = new User({
		// 	username : 'admin',
		// 	password : 'admin2017@',
		// 	user_type : 30
		// });
		// User.createUser(adminUser, function(err, user){
		// 	if(err) throw err;
		// });
	}
});

//User Login
passport.use(new LocalStrategy(
  function(username, password, done) {
    User.getUserByUsername(username, function(err, user){
    	if(err) throw err;
    	if(!user){
    		return done(null , false,{message: 'User cannot be found!'});
    	}
    	User.comparePassword(password, user.password, function(err, isMatch){
    		if(err) throw err;
    		if(isMatch){
    			return done(null , user);
    		}
    		else{
    			return done(null, false, {message:'Invalid Password.'});
    		}
    	});
    });
  }));

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.getUserById(id, function(err, user) {
    done(err, user);
  });
});


router.post('/login',
  passport.authenticate('local', {successRedirect: '/' , failureRedirect: '/users/login' , failureFlash: true }),
  function(req, res) {
  	res.redirect('/');
});

router.get('/logout', function(req, res){
	req.logout();
	req.flash('success_msg', 'You have logged out.');
	res.redirect('/users/login');
});

//View Student List
router.post("/studentList" , function(req , res){
	User.find({user_type:"3"}, function(err, users) {
	  if (err) throw err;
	  // object of all the users
	  res.render('studentlist',{data:users});
	});
});


//Select Courses
router.post("/apply" , function(req , res){
	var appliedCourse = req.body.course;
	var name = req.user.name;
	var email =  req.user.email;
	console.log(appliedCourse,name,email);
	var userCourse = new UserCourse({
		name: name,
		email: email,
		course_name: appliedCourse,
	});
	UserCourse.insertOne(userCourse, function(err, usercourse){
		if(err) throw err;
		console.log(userCourse);
	})
	req.flash('success_msg' , 'You have successfully registered for ' + appliedCourse);
	res.redirect('/');
	
});



router.post("/courseList" , function(req , res){
	Course.find({}, function(err, courses) {
	  if (err) throw err;
	  // object of all the users
	  //console.log(courses);
	  res.render('courseListStudents',{data:courses});
	});
});


module.exports = router;