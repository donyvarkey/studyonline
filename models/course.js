var mongoose =  require('mongoose');


//CourseScema
var CourseSchema = mongoose.Schema({
	course_name: {
		type: String
	},
	course_dur: {
		type: Number
	},
	instructor: {
		type: String
	}
});

var Course = module.exports = mongoose.model('Course', CourseSchema);
module.exports.insertOne = function(newCourse, callback){
	newCourse.save(callback);
}