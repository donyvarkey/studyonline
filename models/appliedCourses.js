var mongoose =  require('mongoose');


//CourseScema
var UserCourseSchema = mongoose.Schema({
	name: {
		type: String
	},
	email: {
		type: String
	},
	course_name: {
		type: String
	}
});

var UserCourse = module.exports = mongoose.model('UserCourse', UserCourseSchema);
module.exports.insertOne = function(userCourse, callback){
	userCourse.save(callback);
}