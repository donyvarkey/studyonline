var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var exphbs = require('express-handlebars');
var expressValidator = require('express-validator');
var session = require('express-session');
var flash = require('connect-flash');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongo = require('mongodb');
var mongoose = require('mongoose');
var notifier = require('node-notifier');

//Connect with mongodb
mongoose.connect('mongodb://localhost/loginapp');
var db = mongoose.connection;
 

var routes = require('./routes/index');
var users = require('./routes/users');
var admin = require('./routes/admin');



//Init app
var app = express();

//Handlebar engine  config
app.set('views' , path.join(__dirname, 'views'));	//set path to views dir
app.engine('handlebars', exphbs({defaultLayout:"layout"}));	//set app engine
app.set('view engine', 'handlebars');				


//bodyParser and cookieParser config
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

//Set static dir
app.use(express.static(path.join(__dirname, 'public')));


//Express session
app.use(session({
	secret: 'myself',
	saveUninitialized: true,
	resave: true
}));

//Passport init
app.use(passport.initialize());
app.use(passport.session());



//Express Validator
app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
      var namespace = param.split('.')
      , root    = namespace.shift()
      , formParam = root;

    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param : formParam,
      msg   : msg,
      value : value
    };
  }
}));


//Connect Flash
app.use(flash());

//Global variables for msg display
app.use(function(req ,res, next){
	res.locals.success_msg = req.flash('success_msg');
	res.locals.err_msg = req.flash('err_msg');
	res.locals.err = req.flash('err');
	res.locals.user = req.user || null;
	next();
});

app.use('/', routes);
app.use('/users', users);
app.use('/admin' , admin);

//Setting port
app.set('port', (process.env.PORT || 3000));
app.listen(app.get('port'), function(){
	console.log('Server has started at port ' + app.get('port'));
});











